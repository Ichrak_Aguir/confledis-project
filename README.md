Recrutement Stagiaire Développeur H/F 2023 Phase 2 - Test Technique 

Ce projet est une application  Reactjs qui permettant de créer/modifier/supprimer un produit et d’afficher la liste de produits avec la possibilité de faire un recherche par nom

💻 Installation

Pour installer l'application, suivez ces étapes :

Clonez le projet de GitLab sur votre ordinateur local.

Accédez au répertoire du projet et exécutez npm install -g json-server et npm install pour installer les dépendances requises.

Démarrez le serveur de développement en exécutant json-server --watch db.json et  npm start en même temps

🔬 Documentation

API :
GET /products
Cet api renvoie une liste de tous les produits dans la fichier db. Chaque produit est représenté sous forme d'un objet JSON avec les propriétés suivantes :
id : L'identifiant unique de produit.
name : le nom de produit.
price : le prix unitaire de produit.
quantity : la quantité de produit.

POST /products Cette api permet de créer un nouveau produit.

PUT /products/id Cette api permet de modifier un produit.

DELETE /products/id Cette api permet de supprimer un produit.

Interface gestion des produits :

Dans cette interface , vous pouvez voir la liste de tous les produits existantes. Vous pouvez créer un nouveau produit en cliquant sur le bouton "Ajouter un produit" en haut à droite de la page. Pour modifier ou supprimer un produit existante, cliquez sur le bouton correspondant.

Pour créer ou modifier un produit, vous devez remplir le formulaire avec le nom, le prix et la quantité.


🎉Conclusion
C'est ça! Vous devriez maintenant avoir une application ReactJs  qui peut créer, mettre à jour, supprimer et afficher un produit. 😄

💬Contact
Si vous avez des questions ou des commentaires sur ce projet, n'hésitez pas à me contacter à ichrakaguir123@gmail.com. J'aimerais recevoir de vos nouvelles !