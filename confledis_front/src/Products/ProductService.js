export const ProductService = {
  url: "http://localhost:3000/products",
  // fetch all product
  async getProducts(payload) {
    let urlGet = payload ? this.url + "?name=" + payload.name : this.url;
    const response = await fetch(urlGet);
    if (!response.ok) {
      throw new Error("Something went wrong!");
    }
    return response.json();
  },

  async addNewProduct(payload) {
    const response = await fetch(this.url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });
    if (!response.ok) {
      throw new Error("Something went wrong!");
    }
    return response.json();
  },
  async updateProduct(payload) {
    const response = await fetch(this.url + "/" + payload.id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    });

    if (!response.ok) {
      throw new Error("Something went wrong!");
    }

    return response.json();
  },
  async deleteProduct(id) {
    const response = await fetch(this.url + "/" + id, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      throw new Error("Something went wrong!");
    }

    return true;
  },
};
