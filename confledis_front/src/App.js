import React from 'react';
import './App.css';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Product from './component/Product';

function App() {
    return (
      <BrowserRouter>
        {/* <Header /> */}
        <Routes>
          <Route path="" element={<Product />} />
        </Routes>
      </BrowserRouter>
    );
}

export default App;
