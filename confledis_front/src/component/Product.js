import React, { useState, useEffect, useRef } from "react";
import { classNames } from "primereact/utils";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { ProductService } from "../Products/ProductService";
import { Toast } from "primereact/toast";
import { Button } from "primereact/button";
import { Toolbar } from "primereact/toolbar";
import { InputNumber } from "primereact/inputnumber";
import { Dialog } from "primereact/dialog";
import { InputText } from "primereact/inputtext";
import "./Product.css";
import { BiCheckShield } from "react-icons/bi";

export default function ProductsDemo() {
  let emptyProduct = {
    name: "",
    price: 0,
    quantity: 0,
  };

  const [products, setProducts] = useState(null);
  const [productDialog, setProductDialog] = useState(false);
  const [deleteProductDialog, setDeleteProductDialog] = useState(false);
  const [product, setProduct] = useState(emptyProduct);
  const [selectedProducts, setSelectedProducts] = useState(null);
  const [submitted, setSubmitted] = useState(false);
  const toast = useRef(null);
  const dt = useRef(null);

  useEffect(() => {
    ProductService.getProducts().then((data) => setProducts(data));
  }, []);

  const formatCurrency = (value) => {
    return value.toLocaleString("en-US", {
      style: "currency",
      currency: "USD",
    });
  };

  const openNew = () => {
    setProduct(emptyProduct);
    setSubmitted(false);
    setProductDialog(true);
  };

  const hideDialog = () => {
    setSubmitted(false);
    setProductDialog(false);
  };

  const hideDeleteProductDialog = () => {
    setDeleteProductDialog(false);
  };

  const saveProduct = async () => {
    setSubmitted(true);
    if (product.name.trim()) {
      if (product.id) {
        const resUpdate = await ProductService.updateProduct(product);
        if (resUpdate) {
          const index = products.findIndex((i) => i.id === product.id);
          if (index >= 0) {
            products[index] = resUpdate;
            toast.current.show({
              icon: "pi pi-check",
              severity: "success",
              summary: "Succès",
              detail: "Produit Modifié",
              life: 3000,
            });
          }
        }
      } else {
        const response = await ProductService.addNewProduct(product);
        if (response) {
          products.push(response);
          toast.current.show({
            severity: "success",
            summary: "Succès",
            detail: "Produit Crée",
            life: 3000,
          });
        }
      }
      setProductDialog(false);
      setProduct(emptyProduct);
    }
  };

  const editProduct = (product) => {
    setProduct({ ...product });
    setProductDialog(true);
  };

  const confirmDeleteProduct = (product) => {
    setProduct(product);
    setDeleteProductDialog(true);
  };

  const deleteProduct = async () => {
    const response = await ProductService.deleteProduct(product.id);
    if (response) {
      setProducts(products.filter((i) => i.id !== product.id));
      setDeleteProductDialog(false);
      setProduct(emptyProduct);
      toast.current.show({
        severity: "success",
        summary: "Succès",
        detail: "Produit Supprimé",
        life: 3000,
      });
    }
  };

  const onInputChange = (e, name) => {
    const val = (e.target && e.target.value) || "";
    let _product = { ...product };

    _product[`${name}`] = val;

    setProduct(_product);
  };

  const onInputNumberChange = (e, name) => {
    const val = e.value || 0;
    let _product = { ...product };

    _product[`${name}`] = val;

    setProduct(_product);
  };

  const rightToolbarTemplate = () => {
    return (
      <div className="flex flex-wrap gap-2 ajout">
        <Button
          label="Ajouter un produit"
          icon="pi pi-plus-circle"
          severity="success"
          onClick={openNew}
          className="addButton"
        />
      </div>
    );
  };

  const priceBodyTemplate = (rowData) => {
    return formatCurrency(rowData.price);
  };

  const actionBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <Button
          icon="pi pi-pencil"
          rounded
          outlined
          className="mr-2 btnAction"
          onClick={() => editProduct(rowData)}
        />
        <Button
          icon="pi pi-trash"
          rounded
          outlined
          severity="danger"
          className="btnAction"
          onClick={() => confirmDeleteProduct(rowData)}
        />
      </React.Fragment>
    );
  };

  const header = (
    <div className="flex flex-wrap gap-2 align-items-center justify-content-between">
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          className="inputSearch"
          type="search"
          onInput={(e) =>
            ProductService.getProducts({ name: e.target.value }).then((data) =>
              setProducts(data)
            )
          }
          placeholder="Chercher ..."
        />
      </span>
    </div>
  );
  const productDialogFooter = (
    <React.Fragment>
      <Button
        label="Annuler"
        icon="pi pi-times-circle"
        outlined
        onClick={hideDialog}
      />
      <Button
        label="Enregistrer"
        icon="pi pi-check-circle"
        onClick={saveProduct}
      />
      
    </React.Fragment>
  );
  const deleteProductDialogFooter = (
    <React.Fragment>
      <Button
        label="Non"
        icon="pi pi-times-circle"
        outlined
        onClick={hideDeleteProductDialog}
      />
      <Button
        label="Oui"
        icon="pi pi-check-circle"
        severity="danger"
        onClick={deleteProduct}
      />
    </React.Fragment>
  );
  const title = <div className="title">GESTION DES PRODUITS </div>;

  return (
    <div>
      <Toast ref={toast} />
      <div className="card">
        <Toolbar className="mb-4" left={title}></Toolbar>

        <Toolbar
          className="mb-4"
          left={header}
          right={rightToolbarTemplate}
        ></Toolbar>

        <DataTable
          ref={dt}
          value={products}
          selection={selectedProducts}
          onSelectionChange={(e) => setSelectedProducts(e.value)}
          dataKey="id"
          paginator
          rows={5}
          rowsPerPageOptions={[5, 10, 25]}
          paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
          currentPageReportTemplate="MONTRER {first}-{last} DE {totalRecords} PRODUITS"
        >
          <Column
            field="name"
            header="Nom"
            style={{ minWidth: "23rem" }}
          ></Column>
          <Column
            field="price"
            header="Prix unitaire"
            body={priceBodyTemplate}
            style={{ minWidth: "15rem" }}
          ></Column>
          <Column
            field="quantity"
            header="Quantité"
            style={{ minWidth: "10rem" }}
          ></Column>

          <Column
            body={actionBodyTemplate}
            exportable={false}
            style={{ minWidth: "12rem", textAlignLast: "end" }}
          ></Column>
        </DataTable>
      </div>

      <Dialog
        visible={productDialog}
        style={{ width: "32rem" }}
        breakpoints={{ "960px": "75vw", "641px": "90vw" }}
        header="Détails du produit"
        modal
        className="p-fluid"
        footer={productDialogFooter}
        onHide={hideDialog}
      >
        <div className="field">
          <label htmlFor="name" className="font-bold">
            Nom
          </label>
          <InputText
            id="name"
            value={product.name}
            onChange={(e) => onInputChange(e, "name")}
            required
            autoFocus
            className={classNames({ "p-invalid": submitted && !product.name })}
          />
          {submitted && !product.name && (
            <small className="p-error">Nom Obligatoire.</small>
          )}
        </div>
        <div className="formgrid grid">
          <div className="field col">
            <label htmlFor="price" className="font-bold">
              Prix unitaire
            </label>
            <InputNumber
              id="price"
              value={product.price}
              onValueChange={(e) => onInputNumberChange(e, "price")}
              mode="currency"
              currency="USD"
              locale="en-US"
            />
          </div>
          <div className="field col">
            <label htmlFor="quantity" className="font-bold">
              Quantité
            </label>
            <InputNumber
              id="quantity"
              value={product.quantity}
              onValueChange={(e) => onInputNumberChange(e, "quantity")}
            />
          </div>
        </div>
      </Dialog>

      <Dialog
        visible={deleteProductDialog}
        style={{ width: "32rem" }}
        breakpoints={{ "960px": "75vw", "641px": "90vw" }}
        header="Confirmer"
        modal
        footer={deleteProductDialogFooter}
        onHide={hideDeleteProductDialog}
      >
        <div className="confirmation-content">
          <BiCheckShield
            style={{ color: "#707070", fontSize: "20px", marginRight: "5px" }}
          />
          {product && (
            <span>
              Sur de vouloir supprimer <b>{product.name}</b> ?
            </span>
          )}
        </div>
      </Dialog>
    </div>
  );
}
